# Use Java Runtime Environment ( JRE 17) base image
FROM eclipse-temurin:17-jre-ubi9-minimal

# Set the working directory inside the container
WORKDIR /app

# Copy files
COPY target/ESE-TicTacToe-1.0-SNAPSHOT.jar /app/tictactoe/
COPY target/lib /app/tictactoe/lib/
COPY src/main/webapp/ /app/tictactoe/webapp/

# Expose the default Jetty port
EXPOSE 8080

# CMD instruction to start Jetty
CMD ["java", "-jar", "tictactoe/ESE-TicTacToe-1.0-SNAPSHOT.jar", "localhost", "8080", "/ESE-TicTacToe", "/app/tictactoe/webapp/"]
