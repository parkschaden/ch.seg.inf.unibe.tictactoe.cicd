package ch.seg.inf.unibe.tictactoe.websockets;

import ch.seg.inf.unibe.tictactoe.websockets.server.GameServer;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.out.println("Usage: java Main <address> <port> <contextPath> <webAppPath>");
            System.exit(1);
        }

        // Server settings:
        final String address = args[0];
        final int port = Integer.parseInt(args[1]);
        final String contextPath = args[2];

        // The application can be provided either as a WAR file path or as a webapp folder path:
        final String webAppPath = args[3];

        new GameServer(address, port, contextPath, webAppPath);
    }
}
